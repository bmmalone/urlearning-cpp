This c++ project implements a number of algorithms for learning Bayesian network structures using state space search techniques. Please see the main [urlearning](http://urlearning.org/) site for more details.

**Install**

This package requires [Boost](http://www.boost.org/), including several its compiled libraries. Please see the boost documentation for more details.

The following instructions give one way to install boost.

* Download and unzip the latest boost release.

* Run the bootstrap program with the appropriate prefix: ``./bootstrap.sh --prefix=$HOME/local``

* Compile and install the boost libraries: ``./b2 install``

* Add the necessary paths to ``$HOME/.bashrc``: 
    * ``export C_INCLUDE_PATH=$HOME/local/include:$C_INCLUDE_PATH``
    * ``export CPLUS_INCLUDE_PATH=$HOME/local/include:$C_INCLUDE_PATH``
    * ``export LD_LIBRARY_PATH=$HOME/local/lib:$HOME/local/lib64:$LD_LIBRARY_PATH``
    * ``export LIBRARY_PATH=$LD_LIBRARY_PATH``
    * ``export PATH=$HOME/local/bin:$PATH``
    

* Ensure the default install location exists: ``mkdir $HOME/local/bin``

* Then, close the shell (or run ``source $HOME/.bashrc``)

After boost libraries are properly installed, the ``build.py`` (python3) script can be used to compile these solvers with a command like:

``./build.py astar --create-symlink``

Please try ``./build.py --help`` for more solver and compilation options.

**Usage**

Learning Bayesian networks using this package entails two steps. 

**Calculating local scores**

First, the local scores must be calculated by the ``score`` program. It can be built with a command like:

``./build.py score --create-symlink``

The scores are then calculated from csv files and stored in [pss files](http://urlearning.org/PSS-Keywords.odf) with a command like:

``score iris.csv iris.pss``

The ``score`` program treats each column as a categorical discrete variable, and each unique string is treated as a separate categorical value. In particular, the program *does not* perform any sort of discretization, normalization, etc. Furthermore, it *does not* remove records with missing values. String like "?", "NA", etc., will simply be treated as other categories for the variables for which they appear. Thus, it is likely custom preprocessing scripts will be necessary before using the ``score`` program.

Please use ``score --help`` to see options such as changing the scoring function and setting a limit on the maximum parent set size.

**Learning the structure**

Once the scores are available, the learning algorithms can be used to learn the optimal structures. The package includes four structure learning algorithms:

* ``astar``
* ``bfbnb``
* ``anytime-window-astar``
* ``dfbnb``

They are built as shown above.

The algorithms are called passing in a pss file, and they output relevant information, such as nodes expanded, timing information, etc. If desired, the ``-n`` option can be used to save the learned network in the Hugin network file format. An example using the astar command is:

``astar iris.pss -n iris.net``


The structure is output in the Hugin network format. Please see the [Hugin reference manual](http://download.hugin.com/webdocs/manuals/api-manual.pdf), especially Sections 13.2 and 13.6, for detailed specifications. Currently, URLearning does not include parameter estimatation, so it simply outputs uniform conditional probability tables. In particular, the network files are designed to interoperate with GeNIe/SMILE (now associated with BayesFusion).

The ``--help`` command line option will show more information about each program. For example:

``astar --help``.

**Development**

This is a netbeans c++ project, so that environment is likely the easiest for further development. Netbeans *is not* required for compilation/installation. Please see ``doc/configuring-netbeans.pdf`` and ``doc/adding-new-programs.pdf`` for more details about configuring Netbeans to compile URLearning and adding new programs (such as new learning algorithms).
