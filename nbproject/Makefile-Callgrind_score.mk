#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Callgrind_score
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/MergedTempFile.o \
	${OBJECTDIR}/PreviousLayerStream.o \
	${OBJECTDIR}/ad_node.o \
	${OBJECTDIR}/ad_tree.o \
	${OBJECTDIR}/ad_tree_scoring_main.o \
	${OBJECTDIR}/bayesian_network.o \
	${OBJECTDIR}/bdeu_scoring_function.o \
	${OBJECTDIR}/bic_scoring_function.o \
	${OBJECTDIR}/fnml_scoring_function.o \
	${OBJECTDIR}/hugin_structure_writer.o \
	${OBJECTDIR}/log_likelihood_calculator.o \
	${OBJECTDIR}/score_cache.o \
	${OBJECTDIR}/score_calculator.o \
	${OBJECTDIR}/sparse_parent_tree.o \
	${OBJECTDIR}/tarjans_algorithm.o \
	${OBJECTDIR}/vary_node.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/TestVarset.o

# C Compiler Flags
CFLAGS=-march=native -g

# CC Compiler Flags
CCFLAGS=-std=c++0x -march=native -g
CXXFLAGS=-std=c++0x -march=native -g

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/home/bmmalone/local/lib -lboost_system -lboost_thread -lboost_chrono -lrt -lboost_timer -lboost_program_options

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/score

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/score: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/score ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/MergedTempFile.o: MergedTempFile.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MergedTempFile.o MergedTempFile.cpp

${OBJECTDIR}/PreviousLayerStream.o: PreviousLayerStream.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreviousLayerStream.o PreviousLayerStream.cpp

${OBJECTDIR}/ad_node.o: ad_node.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_node.o ad_node.cpp

${OBJECTDIR}/ad_tree.o: ad_tree.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_tree.o ad_tree.cpp

${OBJECTDIR}/ad_tree_scoring_main.o: ad_tree_scoring_main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_tree_scoring_main.o ad_tree_scoring_main.cpp

${OBJECTDIR}/bayesian_network.o: bayesian_network.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bayesian_network.o bayesian_network.cpp

${OBJECTDIR}/bdeu_scoring_function.o: bdeu_scoring_function.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bdeu_scoring_function.o bdeu_scoring_function.cpp

${OBJECTDIR}/bic_scoring_function.o: bic_scoring_function.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bic_scoring_function.o bic_scoring_function.cpp

${OBJECTDIR}/fnml_scoring_function.o: fnml_scoring_function.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fnml_scoring_function.o fnml_scoring_function.cpp

${OBJECTDIR}/hugin_structure_writer.o: hugin_structure_writer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/hugin_structure_writer.o hugin_structure_writer.cpp

${OBJECTDIR}/log_likelihood_calculator.o: log_likelihood_calculator.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/log_likelihood_calculator.o log_likelihood_calculator.cpp

${OBJECTDIR}/score_cache.o: score_cache.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_cache.o score_cache.cpp

${OBJECTDIR}/score_calculator.o: score_calculator.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_calculator.o score_calculator.cpp

${OBJECTDIR}/sparse_parent_tree.o: sparse_parent_tree.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_tree.o sparse_parent_tree.cpp

${OBJECTDIR}/tarjans_algorithm.o: tarjans_algorithm.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tarjans_algorithm.o tarjans_algorithm.cpp

${OBJECTDIR}/vary_node.o: vary_node.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vary_node.o vary_node.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/TestVarset.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/TestVarset.o: TestVarset.cpp 
	${MKDIR} -p ${TESTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/TestVarset.o TestVarset.cpp


${OBJECTDIR}/MergedTempFile_nomain.o: ${OBJECTDIR}/MergedTempFile.o MergedTempFile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MergedTempFile.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MergedTempFile_nomain.o MergedTempFile.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MergedTempFile.o ${OBJECTDIR}/MergedTempFile_nomain.o;\
	fi

${OBJECTDIR}/PreviousLayerStream_nomain.o: ${OBJECTDIR}/PreviousLayerStream.o PreviousLayerStream.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/PreviousLayerStream.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreviousLayerStream_nomain.o PreviousLayerStream.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/PreviousLayerStream.o ${OBJECTDIR}/PreviousLayerStream_nomain.o;\
	fi

${OBJECTDIR}/ad_node_nomain.o: ${OBJECTDIR}/ad_node.o ad_node.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ad_node.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_node_nomain.o ad_node.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ad_node.o ${OBJECTDIR}/ad_node_nomain.o;\
	fi

${OBJECTDIR}/ad_tree_nomain.o: ${OBJECTDIR}/ad_tree.o ad_tree.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ad_tree.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_tree_nomain.o ad_tree.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ad_tree.o ${OBJECTDIR}/ad_tree_nomain.o;\
	fi

${OBJECTDIR}/ad_tree_scoring_main_nomain.o: ${OBJECTDIR}/ad_tree_scoring_main.o ad_tree_scoring_main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ad_tree_scoring_main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ad_tree_scoring_main_nomain.o ad_tree_scoring_main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ad_tree_scoring_main.o ${OBJECTDIR}/ad_tree_scoring_main_nomain.o;\
	fi

${OBJECTDIR}/bayesian_network_nomain.o: ${OBJECTDIR}/bayesian_network.o bayesian_network.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/bayesian_network.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bayesian_network_nomain.o bayesian_network.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/bayesian_network.o ${OBJECTDIR}/bayesian_network_nomain.o;\
	fi

${OBJECTDIR}/bdeu_scoring_function_nomain.o: ${OBJECTDIR}/bdeu_scoring_function.o bdeu_scoring_function.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/bdeu_scoring_function.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bdeu_scoring_function_nomain.o bdeu_scoring_function.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/bdeu_scoring_function.o ${OBJECTDIR}/bdeu_scoring_function_nomain.o;\
	fi

${OBJECTDIR}/bic_scoring_function_nomain.o: ${OBJECTDIR}/bic_scoring_function.o bic_scoring_function.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/bic_scoring_function.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bic_scoring_function_nomain.o bic_scoring_function.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/bic_scoring_function.o ${OBJECTDIR}/bic_scoring_function_nomain.o;\
	fi

${OBJECTDIR}/fnml_scoring_function_nomain.o: ${OBJECTDIR}/fnml_scoring_function.o fnml_scoring_function.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/fnml_scoring_function.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fnml_scoring_function_nomain.o fnml_scoring_function.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/fnml_scoring_function.o ${OBJECTDIR}/fnml_scoring_function_nomain.o;\
	fi

${OBJECTDIR}/hugin_structure_writer_nomain.o: ${OBJECTDIR}/hugin_structure_writer.o hugin_structure_writer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/hugin_structure_writer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/hugin_structure_writer_nomain.o hugin_structure_writer.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/hugin_structure_writer.o ${OBJECTDIR}/hugin_structure_writer_nomain.o;\
	fi

${OBJECTDIR}/log_likelihood_calculator_nomain.o: ${OBJECTDIR}/log_likelihood_calculator.o log_likelihood_calculator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/log_likelihood_calculator.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/log_likelihood_calculator_nomain.o log_likelihood_calculator.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/log_likelihood_calculator.o ${OBJECTDIR}/log_likelihood_calculator_nomain.o;\
	fi

${OBJECTDIR}/score_cache_nomain.o: ${OBJECTDIR}/score_cache.o score_cache.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/score_cache.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_cache_nomain.o score_cache.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/score_cache.o ${OBJECTDIR}/score_cache_nomain.o;\
	fi

${OBJECTDIR}/score_calculator_nomain.o: ${OBJECTDIR}/score_calculator.o score_calculator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/score_calculator.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_calculator_nomain.o score_calculator.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/score_calculator.o ${OBJECTDIR}/score_calculator_nomain.o;\
	fi

${OBJECTDIR}/sparse_parent_tree_nomain.o: ${OBJECTDIR}/sparse_parent_tree.o sparse_parent_tree.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/sparse_parent_tree.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_tree_nomain.o sparse_parent_tree.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/sparse_parent_tree.o ${OBJECTDIR}/sparse_parent_tree_nomain.o;\
	fi

${OBJECTDIR}/tarjans_algorithm_nomain.o: ${OBJECTDIR}/tarjans_algorithm.o tarjans_algorithm.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tarjans_algorithm.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tarjans_algorithm_nomain.o tarjans_algorithm.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/tarjans_algorithm.o ${OBJECTDIR}/tarjans_algorithm_nomain.o;\
	fi

${OBJECTDIR}/vary_node_nomain.o: ${OBJECTDIR}/vary_node.o vary_node.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/vary_node.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O3 -Wall -I/home/bmmalone/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vary_node_nomain.o vary_node.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/vary_node.o ${OBJECTDIR}/vary_node_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
