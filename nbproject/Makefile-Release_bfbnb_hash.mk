#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release_bfbnb_hash
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/MergedTempFile.o \
	${OBJECTDIR}/PreviousLayerStream.o \
	${OBJECTDIR}/bayesian_network.o \
	${OBJECTDIR}/bfbnb_hash_main.o \
	${OBJECTDIR}/combined_pattern_database.o \
	${OBJECTDIR}/dynamic_pattern_database.o \
	${OBJECTDIR}/file_pattern_database.o \
	${OBJECTDIR}/hugin_structure_writer.o \
	${OBJECTDIR}/score_cache.o \
	${OBJECTDIR}/sparse_parent_bitwise.o \
	${OBJECTDIR}/sparse_parent_list.o \
	${OBJECTDIR}/sparse_parent_tree.o \
	${OBJECTDIR}/static_pattern_database.o \
	${OBJECTDIR}/tarjans_algorithm.o \
	${OBJECTDIR}/top_p_constraint.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/TestVarset.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++11 -march=native
CXXFLAGS=-std=c++11 -march=native

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/local/lib -lboost_timer -lboost_program_options -lboost_system -lboost_chrono -lboost_thread -lboost_serialization

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bfbnb-hash-release

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bfbnb-hash-release: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bfbnb-hash-release ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/MergedTempFile.o: MergedTempFile.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MergedTempFile.o MergedTempFile.cpp

${OBJECTDIR}/PreviousLayerStream.o: PreviousLayerStream.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreviousLayerStream.o PreviousLayerStream.cpp

${OBJECTDIR}/bayesian_network.o: bayesian_network.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bayesian_network.o bayesian_network.cpp

${OBJECTDIR}/bfbnb_hash_main.o: bfbnb_hash_main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bfbnb_hash_main.o bfbnb_hash_main.cpp

${OBJECTDIR}/combined_pattern_database.o: combined_pattern_database.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/combined_pattern_database.o combined_pattern_database.cpp

${OBJECTDIR}/dynamic_pattern_database.o: dynamic_pattern_database.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/dynamic_pattern_database.o dynamic_pattern_database.cpp

${OBJECTDIR}/file_pattern_database.o: file_pattern_database.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/file_pattern_database.o file_pattern_database.cpp

${OBJECTDIR}/hugin_structure_writer.o: hugin_structure_writer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/hugin_structure_writer.o hugin_structure_writer.cpp

${OBJECTDIR}/score_cache.o: score_cache.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_cache.o score_cache.cpp

${OBJECTDIR}/sparse_parent_bitwise.o: sparse_parent_bitwise.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_bitwise.o sparse_parent_bitwise.cpp

${OBJECTDIR}/sparse_parent_list.o: sparse_parent_list.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_list.o sparse_parent_list.cpp

${OBJECTDIR}/sparse_parent_tree.o: sparse_parent_tree.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_tree.o sparse_parent_tree.cpp

${OBJECTDIR}/static_pattern_database.o: static_pattern_database.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/static_pattern_database.o static_pattern_database.cpp

${OBJECTDIR}/tarjans_algorithm.o: tarjans_algorithm.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tarjans_algorithm.o tarjans_algorithm.cpp

${OBJECTDIR}/top_p_constraint.o: top_p_constraint.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/top_p_constraint.o top_p_constraint.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/TestVarset.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/TestVarset.o: TestVarset.cpp 
	${MKDIR} -p ${TESTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -I/usr/local/include -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/TestVarset.o TestVarset.cpp


${OBJECTDIR}/MergedTempFile_nomain.o: ${OBJECTDIR}/MergedTempFile.o MergedTempFile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MergedTempFile.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MergedTempFile_nomain.o MergedTempFile.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MergedTempFile.o ${OBJECTDIR}/MergedTempFile_nomain.o;\
	fi

${OBJECTDIR}/PreviousLayerStream_nomain.o: ${OBJECTDIR}/PreviousLayerStream.o PreviousLayerStream.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/PreviousLayerStream.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreviousLayerStream_nomain.o PreviousLayerStream.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/PreviousLayerStream.o ${OBJECTDIR}/PreviousLayerStream_nomain.o;\
	fi

${OBJECTDIR}/bayesian_network_nomain.o: ${OBJECTDIR}/bayesian_network.o bayesian_network.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/bayesian_network.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bayesian_network_nomain.o bayesian_network.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/bayesian_network.o ${OBJECTDIR}/bayesian_network_nomain.o;\
	fi

${OBJECTDIR}/bfbnb_hash_main_nomain.o: ${OBJECTDIR}/bfbnb_hash_main.o bfbnb_hash_main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/bfbnb_hash_main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bfbnb_hash_main_nomain.o bfbnb_hash_main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/bfbnb_hash_main.o ${OBJECTDIR}/bfbnb_hash_main_nomain.o;\
	fi

${OBJECTDIR}/combined_pattern_database_nomain.o: ${OBJECTDIR}/combined_pattern_database.o combined_pattern_database.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/combined_pattern_database.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/combined_pattern_database_nomain.o combined_pattern_database.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/combined_pattern_database.o ${OBJECTDIR}/combined_pattern_database_nomain.o;\
	fi

${OBJECTDIR}/dynamic_pattern_database_nomain.o: ${OBJECTDIR}/dynamic_pattern_database.o dynamic_pattern_database.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/dynamic_pattern_database.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/dynamic_pattern_database_nomain.o dynamic_pattern_database.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/dynamic_pattern_database.o ${OBJECTDIR}/dynamic_pattern_database_nomain.o;\
	fi

${OBJECTDIR}/file_pattern_database_nomain.o: ${OBJECTDIR}/file_pattern_database.o file_pattern_database.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/file_pattern_database.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/file_pattern_database_nomain.o file_pattern_database.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/file_pattern_database.o ${OBJECTDIR}/file_pattern_database_nomain.o;\
	fi

${OBJECTDIR}/hugin_structure_writer_nomain.o: ${OBJECTDIR}/hugin_structure_writer.o hugin_structure_writer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/hugin_structure_writer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/hugin_structure_writer_nomain.o hugin_structure_writer.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/hugin_structure_writer.o ${OBJECTDIR}/hugin_structure_writer_nomain.o;\
	fi

${OBJECTDIR}/score_cache_nomain.o: ${OBJECTDIR}/score_cache.o score_cache.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/score_cache.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/score_cache_nomain.o score_cache.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/score_cache.o ${OBJECTDIR}/score_cache_nomain.o;\
	fi

${OBJECTDIR}/sparse_parent_bitwise_nomain.o: ${OBJECTDIR}/sparse_parent_bitwise.o sparse_parent_bitwise.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/sparse_parent_bitwise.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_bitwise_nomain.o sparse_parent_bitwise.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/sparse_parent_bitwise.o ${OBJECTDIR}/sparse_parent_bitwise_nomain.o;\
	fi

${OBJECTDIR}/sparse_parent_list_nomain.o: ${OBJECTDIR}/sparse_parent_list.o sparse_parent_list.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/sparse_parent_list.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_list_nomain.o sparse_parent_list.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/sparse_parent_list.o ${OBJECTDIR}/sparse_parent_list_nomain.o;\
	fi

${OBJECTDIR}/sparse_parent_tree_nomain.o: ${OBJECTDIR}/sparse_parent_tree.o sparse_parent_tree.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/sparse_parent_tree.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sparse_parent_tree_nomain.o sparse_parent_tree.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/sparse_parent_tree.o ${OBJECTDIR}/sparse_parent_tree_nomain.o;\
	fi

${OBJECTDIR}/static_pattern_database_nomain.o: ${OBJECTDIR}/static_pattern_database.o static_pattern_database.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/static_pattern_database.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/static_pattern_database_nomain.o static_pattern_database.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/static_pattern_database.o ${OBJECTDIR}/static_pattern_database_nomain.o;\
	fi

${OBJECTDIR}/tarjans_algorithm_nomain.o: ${OBJECTDIR}/tarjans_algorithm.o tarjans_algorithm.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tarjans_algorithm.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tarjans_algorithm_nomain.o tarjans_algorithm.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/tarjans_algorithm.o ${OBJECTDIR}/tarjans_algorithm_nomain.o;\
	fi

${OBJECTDIR}/top_p_constraint_nomain.o: ${OBJECTDIR}/top_p_constraint.o top_p_constraint.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/top_p_constraint.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -s -I/usr/local/include -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/top_p_constraint_nomain.o top_p_constraint.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/top_p_constraint.o ${OBJECTDIR}/top_p_constraint_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
